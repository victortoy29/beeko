<?php
namespace libs;

class router {    
    public $api = false;
    public $controlador = 'main';
    public $metodo = 'index';
    public $parametros = array();

    public function __construct() {
        if(isset($_GET['url'])) {
            $this->url = filter_input(INPUT_GET, 'url', FILTER_SANITIZE_URL);            
            $this->url = explode('/', $_GET['url']);
            if(!empty($this->url)) {
                //Se evalua si la primera palabra es API si no es asi, es porque es una vista
                if($this->url[0] == 'api') {
                    $this->api = true;
                    array_shift($this->url);
                }
                if(!empty($this->url)) {
                    $this->controlador = array_shift($this->url);
                    if(!empty($this->url)) {
                        $this->metodo = array_shift($this->url);
                        if(!empty($this->url)) {
                            $this->parametros = $this->url;
                        }
                    }
                }
            }
        }
        $this->handle();
    }

    public function handle() {        
        try {
            //Si API=true es porque es una script del backend si no, es una vista
            if($this->api) {
                require_once "controllers/".$this->controlador.".php";
                $objeto = new $this->controlador();
                echo json_encode($objeto->{$this->metodo}($_POST));
            } else {                
                require_once "views/".$this->controlador."/".$this->metodo.".php";
            }
        } catch (Exception $e) {
            header("Location: /genesis_tech/error");
        }
    }
}