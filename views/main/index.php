<?php require('views/header.php');?>

<main role="main" class="container-fluid">
  <div class="jumbotron d-flex justify-content-center text-white">
    <h3>SÉ PARTE DEL CAMBIO QUE REQUIERE NUESTRO PLANETA</h3>
  </div>
</main>
<div class="container" id="contenido"></div>

<?php require('views/footer.php');?>

<script type="text/javascript">
    $(function(){
        let fila = ''
        let temp = 0
        enviarPeticion('colonias', 'select', {estado:'Activo'}, function(r){
            for(let i = 0; i < r.data.length; i++){
                if(i % 3 == 0){
                    fila += '<div class="card-deck mt-2">'
                }
                temp++
                fila += '<div class="card">'+
                            '<img src="assets/img/colonias/colonia'+r.data[i].id+'.jpg" class="card-img-top" alt="...">'+
                            '<div class="card-body">'+
                                '<h5 class="card-title">'+r.data[i].nombre+'</h5>'+
                                '<p class="card-text">'+r.data[i].descripcion+'</p>'+
                            '</div>'+
                            '<div class="card-footer">'+
                                '<small class="text-muted">Last updated 3 mins ago</small>'+
                            '</div>'+
                        '</div>'
                if(temp == 3){
                    fila += '</div>'
                    temp = 0
                }
            }
            $('#contenido').append(fila)
        })
    })
</script>
</body>
</html>